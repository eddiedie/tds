// Fill out your copyright notice in the Description page of Project Settings.


#include "SprintPoint.h"
#include "Character/TDSCharacter.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
ASprintPoint::ASprintPoint()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PointMeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PointMeshComponent"));
	RootComponent = PointMeshComp;

}

// Called when the game starts or when spawned
void ASprintPoint::BeginPlay()
{
	Super::BeginPlay();

	// PointMeshComp->OnComponentBeginOverlap.AddDynamic(this,&ASprintPoint::HandleBeginOverlap);
	// PointMeshComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	// PointMeshComp->SetCollisionResponseToAllChannels(ECR_Overlap);
	
}

// Called every frame
void ASprintPoint::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// void ASprintPoint::HandleBeginOverlap
// 			(
// 				UPrimitiveComponent*		OverlappedComponent,
// 				AActor*						OtherActor,
// 				UPrimitiveComponent*		OtherComponent,
// 				int32						OtherBodyIndex,
// 				bool						bFromSweep,
// 				const FHitResult&			SweepResult
// 			)
// {
// 	
// }