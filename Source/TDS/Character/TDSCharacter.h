// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"

#include "Components/SplineComponent.h"
#include "TDS/FunctionLibrary/Types.h"

#include "TDSCharacter.generated.h"




class UCapsuleComponent;
class ASprintPoint;
class USplineComponent;
class UAudioComponent;
class AWeaponDefault;




UCLASS(Blueprintable)
class ATDSCharacter : public ACharacter
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

public:
	ATDSCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	/*------------------------------- Base Movement ------------------------------------*/
	// Variables >>--------------

	float AxisX = 0;
	float AxisY = 0;


	// Functions >>--------------
	
	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

	UFUNCTION()
	void InputAxisX(float value);

	UFUNCTION()
	void InputAxisY(float value);
	
	UFUNCTION()
	void MovementTick(float DeltaTime);




	/*------------------------------- Movement States ------------------------------------*/
	// Variables >>--------------
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Movement")
	EMovementState MovementState = EMovementState::Run_State;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Movement")
	FCharacterSpeed SpeedInfo;


	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Movement")
	bool AimEnabled = false;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Movement")
	bool WalkEnabled = false;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Movement")
	bool SprintEnabled = false;


	// Functions >>--------------
	
	UFUNCTION(BlueprintCallable)
	void CharacterUpdate();

	UFUNCTION(BlueprintCallable)
	void ChangeMovementState();
	
	



	/*------------------------------- Cursor ------------------------------------*/
	// Variables >>--------------

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Cursor")
	class UMaterialInterface* CursorMaterial;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Cursor")
	FVector CursorSize = FVector(20,40,40);
	
	class UDecalComponent* CurrentCursor = nullptr;



	// Functions >>--------------

	UFUNCTION(BlueprintCallable)
	UDecalComponent* GetCursorToWorld();




	/*------------------------------- Sprint to Cursor ------------------------------------*/
	// Variables >>--------------

	UPROPERTY(VisibleAnywhere,Category="Trigger Capsule")	UCapsuleComponent* TriggerCapsule;
	UPROPERTY(BlueprintReadWrite)							ASprintPoint* SprintPoint;
	UPROPERTY(EditDefaultsOnly)								TSubclassOf<ASprintPoint>SprintPointClass;
	UPROPERTY()												TArray<ASprintPoint*> SprintArray;

	UPROPERTY(BlueprintReadWrite,Category="Sprint Point")												int32 ClickNum = 5;


	

	// Functions >>--------------

	bool EnableRotation = true;
	
	UFUNCTION()
	void OnOverlapBegin
			(
				UPrimitiveComponent*		OverlappedComponent,
				AActor*						OtherActor,
				UPrimitiveComponent*		OtherComponent,
				int32						OtherBodyIndex,
				bool						bFromSweep,
				const FHitResult&			SweepResult
			);

	UFUNCTION()
	void OnOverlapEnd
			(
				UPrimitiveComponent*		OverlappedComponent,
				AActor*						OtherActor,
				UPrimitiveComponent*		OtherComponent,
				int32						OtherBodyIndex
			);

	

	void AddSprintPoints();

	void GoToSprintPoints();

	void RemoveSprintPoints();

	void RemoveAllSprintPoints();



	UPROPERTY(VisibleAnywhere,BlueprintReadOnly,Category="Spline")
	USplineComponent* SplineComponent;

	UFUNCTION(BlueprintCallable)
		void CreateSpline();


	UPROPERTY(VisibleAnywhere,BlueprintReadWrite)
	UAudioComponent* AudioComponent;
	

	


	

	/*------------------------------- Weapon Settings ------------------------------------*/
	// Variables >>--------------
	
	AWeaponDefault* CurrentWeapon = nullptr;
	
	UPROPERTY (EditAnywhere, BlueprintReadWrite, Category = "Demo")
	FName InitWeaponName;


	// Functions >>--------------
	
	UFUNCTION(BlueprintCallable)
	AWeaponDefault* GetCurrentWeapon();

	UFUNCTION(BlueprintCallable)
	void InitWeapon(FName IdWeaponName);

	
	UFUNCTION()
	void InputAttackPressed();
	UFUNCTION()
	void InputAttackReleased();

	

	UFUNCTION(BlueprintCallable)
	void AttackCharEvent(bool bIsFiring);

	UFUNCTION(BlueprintCallable)
	void TryReloadWeapon();



	
	/*------------------------------- Delegates ------------------------------------*/
	UFUNCTION()
	void WeaponReloadStart(UAnimMontage* AnimReload);
	UFUNCTION()
	void WeaponReloadEnd();

	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadStart_BP(UAnimMontage* AnimReload);
	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadEnd_BP();


	UFUNCTION()
	void WeaponFire(UAnimMontage* AnimFire);

	UFUNCTION(BlueprintNativeEvent)
	void WeaponFire_BP(UAnimMontage* AnimFire);



	

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	//FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

	private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	//class UDecalComponent* CursorToWorld;
};

