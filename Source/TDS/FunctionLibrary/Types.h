// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/DataTable.h"

#include "Types.generated.h"



class AProjectileDefault;
class AWeaponDefault;
class USoundBase;
class UParticleSystem;
class UDecalComponent;
class UAnimMontage;
class UStaticMesh;



 UCLASS()
class TDS_API UTypes : public UBlueprintFunctionLibrary
 {
 	GENERATED_BODY()
 };

UENUM(BlueprintType)
enum class EMovementState : uint8
{
	AimRun_State UMETA(DisplayName = "AimRun State"),
	AimWalk_State UMETA(DisplayName = "AimWalk State"),
	Walk_State UMETA(DisplayName = "Walk State"),
	Run_State UMETA(DisplayName = "Run State"),
	Sprint_State UMETA(DisplayName = "Sprint State")
};

USTRUCT(BlueprintType)
struct FCharacterSpeed
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Movement")
	float AimRunSpeed = 200;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Movement")
	float AimWalkSpeed = 75;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Movement")
	float WalkSpeed = 200;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Movement")
	float RunSpeed = 600;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Movement")
	float SprintSpeed = 1000;
 
};




/*---------------------------- Bullet Settings -----------------------------*/

USTRUCT(BlueprintType)
struct FProjectileInfo
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Projectile Settings")
	TSubclassOf<AProjectileDefault> Projectile = nullptr;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Projectile Settings")
	float ProjectileDamage = 20;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Projectile Settings")
	float ProjectileLifeTime = 20;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Projectile Settings")
	float ProjectileInitSpeed = 2000;

	// ���������� ��� �����
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Projectile Settings")
	bool bIsLikeBomb = false;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Projectile Settings")
	float ProjectileMaxRadiusDamage = 200;
};




/*---------------------------- Weapon Settings -----------------------------*/

USTRUCT(BlueprintType)
struct FWeaponDispersion
{
	GENERATED_BODY()
	
	// AimWalk	
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Dispersion")
	float AimWalk_StateDispersionAimMax = 3.5;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Dispersion")
	float AimWalk_StateDispersionAimMin = 0.5;

	//UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Dispersion")
	//float AimWalk_StateDispersionAimRecoil = 1;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Dispersion")
	float AimWalk_StateDispersionReduction = 0.25;

	// AimRun	
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Dispersion")
	float AimRun_StateDispersionAimMax = 5;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Dispersion")
	float AimRun_StateDispersionAimMin = 0.75;

	//UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Dispersion")
	//float AimRun_StateDispersionAimRecoil = 1;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Dispersion")
	float AimRun_StateDispersionReduction = 0.25;

	// Walk	
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Dispersion")
	float Walk_StateDispersionAimMax = 7.5;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Dispersion")
	float Walk_StateDispersionAimMin = 1;

	//UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Dispersion")
	//float Walk_StateDispersionAimRecoil = 1;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Dispersion")
	float Walk_StateDispersionReduction = 0.25;

	// Run	
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Dispersion")
	float Run_StateDispersionAimMax = 10;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Dispersion")
	float Run_StateDispersionAimMin = 2.5;

	//UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Dispersion")
	//float Run_StateDispersionAimRecoil = 1;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Dispersion")
	float Run_StateDispersionReduction = 0.25;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Dispersion")
	float DispersionRecoil = 1;

		
};




USTRUCT(BlueprintType)
struct FWeaponInfo : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Weapon Trace")
	float WeaponDamage = 20;	// ���� � ��� ��� ����, �� ����� ������������ trace ������
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Weapon Trace")
	float DistanceTrace = 2000;
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Weapon State")
	float RateOfFire = 0.5f;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Weapon State")
	float ReloadTime = 4;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Weapon State")
	int32 MaxRound = 10;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Weapon State")
	int32 NumberProjectileByShot = 5;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Weapon Sound")
	USoundBase* SoundFireWeapon = nullptr;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Weapon Sound")
	USoundBase* SoundReloadWeapon = nullptr;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Weapon FX")
	UParticleSystem* EffectFireWeapon = nullptr;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Weapon Hit Effect")
	UDecalComponent* DecalOnHit = nullptr;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Weapon Anim")
	UAnimMontage* AnimCharacterFire = nullptr;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Weapon Anim")
	UAnimMontage* AnimCharacterReload = nullptr;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Weapon Mesh")
	UStaticMesh* MagazineDrop = nullptr;	// ����������� ������� ��� �����������
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Weapon Mesh")
	UStaticMesh* SleeveBullets = nullptr;	// ����� ������ ��� ��������

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Weapon Class")
	TSubclassOf<AWeaponDefault> WeaponClass = nullptr;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Weapon Dispersion")
	FWeaponDispersion WeaponDispersion;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Weapon Projectile Settings")
	FProjectileInfo ProjectileSettings; // ��������� �������� � ���� ��������� FProjectileInfo, ��� ����
										// If null use trace logic  (TSubclassOf<AProjectileDefault>Projectile = nullptr)
};



USTRUCT(BlueprintType) // ��������� ����� ��������� ���� � �������� � �.�.
struct FAdditionalWeaponInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere,BlueprintReadWrite, Category="Weapon Stats")
	int32 Round = 10;	// ���������� �������� � ��������
};
