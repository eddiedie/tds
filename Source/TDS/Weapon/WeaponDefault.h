// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "Components/ArrowComponent.h"
#include "TDS/Weapon/ProjectileDefault.h"
#include "TDS/FunctionLibrary/Types.h"

#include "WeaponDefault.generated.h"


class USceneComponent;			// �����							- SceneComponent
class USkeletalMeshComponent;	// ������ �� �������� � ���������	- SkeletalMeshWeapon
class UStaticMeshComponent;		// ������ �������, ��� ��������		- StaticMeshWeapon	
class UArrowComponent;			// ����������� ��������				- ShootLocation



DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponReloadStart,UAnimMontage*, AnimReload);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnWeaponReloadEnd);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponFire,UAnimMontage*, AnimFire);


UCLASS()
class TDS_API AWeaponDefault : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeaponDefault();


	FOnWeaponReloadStart	OnWeaponReloadStart;
	FOnWeaponReloadEnd		OnWeaponReloadEnd;

	FOnWeaponFire			OnWeaponFire;


	// �����
	UPROPERTY(VisibleAnywhere,BlueprintReadOnly,meta=(AllowPrivateAccess = "true"),Category="Components")
	USceneComponent* SceneComponent = nullptr;

	// ������ �� �������� � ���������
	UPROPERTY(VisibleAnywhere,BlueprintReadOnly,meta=(AllowPrivateAccess = "true"),Category="Components")
	USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;

	// ������ �������, ��� ��������
	UPROPERTY(VisibleAnywhere,BlueprintReadOnly,meta=(AllowPrivateAccess = "true"),Category="Components")
	UStaticMeshComponent* StaticMeshWeapon = nullptr;

	// ����������� ��������
	UPROPERTY(VisibleAnywhere,BlueprintReadOnly,meta=(AllowPrivateAccess = "true"),Category="Components")
	UArrowComponent* ShootLocation = nullptr;


	

	// ��������� ���������� ������ �� Types.h
	UPROPERTY()
	FWeaponInfo WeaponSettings;

	// ��������� �� Types.h, � ������� �� ����� ������� ���������� �������� � ������ � �.�.
	UPROPERTY(EditAnywhere, BlueprintReadWrite,Category="Weapon Settings")
	FAdditionalWeaponInfo AdditionalWeaponInfo;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
	bool WeaponFiring = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite,Category="ReloadLogic")
	bool WeaponReloading = false;


	//Timers'flags
	// �� ��������� ����� 0 - ��� ��������� ���������� ������ ������� ����� �� �������.
	// ����� �� ���������������� ��������� RateOfFire � Types.h
	float FireTimer = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite,Category="ReloadLogic")
	float ReloadTimer = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite,Category="ReloadLogic")
	float tempReloadTimer = 0.0f;

	FVector ShootEndLocation = FVector(0);
	FRotator FRShootRotation;
	


	bool BlockFire = false;

		
	bool ShouldReduceDispersion = false;
	float CurrentDispersion = 0.0f;
	float CurrentDispersionMax = 1;
	float CurrentDispersionMin = 0.1;
	float CurrentDispersionRecoil = 0.1;
	float CurrentDispersionReduction = 0.1;



	//UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Debug")
	//bool byBarrel;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Debug")
	bool ShowDebug;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Debug")
	float SizeVectorToChangeShootDirectionLogic = 100;


	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	void WeaponInit();

	
	UFUNCTION(BlueprintCallable)
	void SetWeaponStateFire(bool bSetFire); // ����������� ������ � ����� ��������, ���� ������ ����� ��������

	bool CheckWeaponCanFire();

	void FireTick(float DeltaSeconds);

	void ReloadTick(float DeltaSeconds);

	UFUNCTION()
	void Fire();

	/*---------------------- Dispersion ---------------------------*/
	FVector ApplyDispersionToShoot(FVector DirectionShoot) const;
	FVector GetFireEndLocation() const;
	float GetCurrentDispersion() const;
	void ChangeDispersionByShot();
	void DispersionTick(float DeltaSeconds);

	FProjectileInfo GetProjectile();

	
	

	void UpdateStateWeapon(EMovementState NewMovementState);
	
	void ChangeDispersion();

	/*---------------------- Bullets ---------------------------*/
	int8 GetNumberProjectileByShot() const;
	
	UFUNCTION(BlueprintCallable)
	int32 GetWeaponRound();

	void InitReload();

	void FinishReload();

};
