// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponDefault.h"

#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "TDS/FunctionLibrary/Types.h"
#include "TDS/Weapon/ProjectileDefault.h"

// Sets default values
AWeaponDefault::AWeaponDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SCeneComponent"));
	RootComponent = SceneComponent;

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMeshWeapon"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName("NoCollision");
	SkeletalMeshWeapon->SetupAttachment(RootComponent);
	SkeletalMeshWeapon->SetRelativeRotation(FRotator(0,-90,0));
	
	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshWeapon"));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName("NoCollision");
	StaticMeshWeapon->SetupAttachment(RootComponent);
	StaticMeshWeapon->SetRelativeRotation(FRotator(0,-90,0));

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	ShootLocation->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void AWeaponDefault::BeginPlay()
{
	Super::BeginPlay();
	WeaponInit();
	if (ShootLocation)
	{
		UE_LOG(LogTemp,Warning,TEXT("ShootLocation"));
	}
}

// Called every frame
void AWeaponDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	FireTick(DeltaTime);
	ReloadTick(DeltaTime);
	DispersionTick(DeltaTime);

	//GEngine->AddOnScreenDebugMessage(-1,1,FColor::Black,FString::Printf(TEXT("CurrentDispersion: %f"),CurrentDispersion));
	//GEngine->AddOnScreenDebugMessage(-1,1,FColor::Black,FString::Printf(TEXT("ShootEndLocation: %s"),*ShootEndLocation.ToString()));
}





void AWeaponDefault::WeaponInit()
{
	// ����� �� ��������� ������� ����� ��������������� ������������, ������ ���������:
	// ���� � ��� ���� USkeletalMeshComponent, � � ���� ��� SkeletalMesh �� �������� ���� ���������
	if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh)
	{
		SkeletalMeshWeapon->DestroyComponent(true);
	}
	if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())
	{
		StaticMeshWeapon->DestroyComponent();
	}
	// ���� � ��� ���� UStaticMeshComponent, � � ���� ��� StaticMesh �� �������� ���� ���������
	//WeaponInfo.Round = WeaponSettings.MaxRound;
	
}




void AWeaponDefault::SetWeaponStateFire(bool bSetFire)
{
	if (CheckWeaponCanFire())	// if BlockFire == false (because the function returns !BlockFire) 
		{
		WeaponFiring = bSetFire;
		}
	else
	{
		WeaponFiring = false;
		FireTimer = 0.01f;
	}
}




bool AWeaponDefault::CheckWeaponCanFire()
{
	return !BlockFire;
}




void AWeaponDefault::FireTick(float DeltaSeconds)
{
	if (GetWeaponRound() > 0)
	{
		if (WeaponFiring)
		{
			if (FireTimer < 0)
			{
				if (!WeaponReloading)
				{
					Fire();
				}
			}
			else
			{
				FireTimer -= DeltaSeconds;
			}
		}
	}
	else
	{
		if (!WeaponReloading)	// ���� ����������� �� ����������, �� ��������� �����������
			{
			InitReload();
			}
	}
}




void AWeaponDefault::Fire()
{
	UAnimMontage* AnimToPlay = nullptr;
	AnimToPlay = WeaponSettings.AnimCharacterFire;
	
	OnWeaponFire.Broadcast(AnimToPlay);
	
	FireTimer = WeaponSettings.RateOfFire;
	AdditionalWeaponInfo.Round -= 1;
	ChangeDispersionByShot();

	UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponSettings.SoundFireWeapon,ShootLocation->GetComponentLocation(),FRotator::ZeroRotator, 1,1,0,nullptr,nullptr,true);
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(),WeaponSettings.EffectFireWeapon,ShootLocation->GetComponentTransform());

	int8 NumberProjectile = GetNumberProjectileByShot();	// ���������� ���� �� ������� �� ���������
	
	if (ShootLocation)
	{
		FVector SpawnLocation = ShootLocation->GetComponentLocation();
		FRotator SpawnRotation = ShootLocation->GetComponentRotation();

		FVector EndLocation;
		for (int i = 0; i < NumberProjectile; i++)
		{
			EndLocation= GetFireEndLocation();
		
			FVector ShootDirection = EndLocation - SpawnLocation;	// ShootEndLocation = HitResult in MovementTick
			ShootDirection.Normalize();
		
			// FRotator SpawnRotation = FRShootRotation;
			FMatrix myMatrix(ShootDirection,FVector(0,1,0),FVector(0,0,1),FVector::ZeroVector);
			SpawnRotation = myMatrix.Rotator();		

			FProjectileInfo ProjectileInfo;
			ProjectileInfo = GetProjectile();
			
			if (ProjectileInfo.Projectile)
			{
				FActorSpawnParameters SpawnParameters;
				SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParameters.Owner = GetOwner();
				SpawnParameters.Instigator = GetInstigator();

				AProjectileDefault* myProjectile = Cast<AProjectileDefault>(GetWorld()->SpawnActor(ProjectileInfo.Projectile,&SpawnLocation,&SpawnRotation,SpawnParameters));
				if (myProjectile)
				{
					myProjectile->InitialLifeSpan = 20;
				}
				else
				{
					//ToDo Projectile null Init trace fire ������� ������ ������ �������� - ��������
				}
			}
		}
		
	}
}




/*---------------------- Dispersion & Aiming ---------------------------*/

FVector AWeaponDefault::GetFireEndLocation() const
{
	bool bShootDirection = false;
	FVector EndLocation = FVector(0.f);
	FVector ShootStartLocation = ShootLocation->GetComponentLocation();
	FVector tempVector = (ShootStartLocation - ShootEndLocation);
	FVector DirectionShoot = (ShootStartLocation - ShootEndLocation).GetSafeNormal();

	// ���� ���������� �� ������ �� ������� ������ ��������� �������, �� ����� �������� ��� ����� �����
	if (tempVector.Size() > SizeVectorToChangeShootDirectionLogic)	
	{
		EndLocation = ShootStartLocation + ApplyDispersionToShoot(DirectionShoot)*-25000;
		if (ShowDebug)
		{
			DrawDebugCone(GetWorld(),ShootStartLocation,-(ShootStartLocation - ShootEndLocation),WeaponSettings.DistanceTrace,GetCurrentDispersion()*PI/180,GetCurrentDispersion()*PI/180,32,FColor::Emerald,false,1,(uint8)'\000',0.5);
		}
	}

	// �� ����� �������� ������ � �������������� ��������� (� ��������� ��������� ������)
	else	
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(ShootLocation->GetForwardVector())*25000;
		if (ShowDebug)
		{
			DrawDebugCone(GetWorld(),ShootStartLocation, ShootLocation->GetForwardVector(),WeaponSettings.DistanceTrace,GetCurrentDispersion()*PI/180,GetCurrentDispersion()*PI/180,32,FColor::Emerald,false,1,(uint8)'\000',0.5);
		}
	}

	if (ShowDebug)
	{
		//Direction weapon look -- ���� ������� ������
		DrawDebugLine(GetWorld(),ShootStartLocation,ShootStartLocation + ShootLocation->GetForwardVector()*500,FColor::Cyan,false,5,(uint8)'\000',1);

		// Direction Projectile must fly -- ���� �� ����� ����������
		DrawDebugLine(GetWorld(),ShootStartLocation,ShootEndLocation,FColor::Red,false,5,(uint8)'\000',0.5);

		// Direction Projectile Current fly -- ���� ���� ����� � ����������
		DrawDebugLine(GetWorld(),ShootStartLocation,EndLocation,FColor::Black,false,10,(uint8)'\000',2);
	}
	
	return EndLocation;
}




/*-- ���������� FMath::VRandCone, ��� � FVector ������������� (-- � ����������� �� ������� GetFireEndLocation() --) �������� ����� �������� ShootLocation � ShootEndLocation --*/
FVector AWeaponDefault::ApplyDispersionToShoot(FVector DirectionShoot) const
{
	return FMath::VRandCone(DirectionShoot,GetCurrentDispersion()*PI/180);
	
	// FVector DirectionShoot - ��� ������ ����������� �������� ��� ��������� ����� ���������� ������. ���� �� �������� ������� ShootLocation;
	// ConeHalfAngleRad - ��� ��������� ������� ���� �������, ��� ��������� GetCurrentDispersion() ����� ����� �� �������;
	// 1� ����� �� ������� ������� 1�*p/180 
}




/*-- �������� ������� ������� ���� �� ������� --*/
float AWeaponDefault::GetCurrentDispersion() const
{
	float Result = CurrentDispersion;
	return Result;
}




/*-- �������� �������, ��������� � ���� ��� � ������ --*/
void AWeaponDefault::ChangeDispersionByShot()
{
	CurrentDispersion += CurrentDispersionRecoil;
}




FProjectileInfo AWeaponDefault::GetProjectile()
{
	return WeaponSettings.ProjectileSettings;
}




void AWeaponDefault::ChangeDispersion()
{
	//CurrentDispersion = CurrentDispersion + CurrentDispersionRecoil;
}




/*-- ����������� ��������� ������ � ������ MovementState ��������� --*/
void AWeaponDefault::UpdateStateWeapon(EMovementState NewMovementState)
{
	BlockFire = false;

	switch (NewMovementState)
	{
	case EMovementState::Run_State:
		CurrentDispersionMax = WeaponSettings.WeaponDispersion.Run_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSettings.WeaponDispersion.Run_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSettings.WeaponDispersion.DispersionRecoil;
		CurrentDispersionReduction = WeaponSettings.WeaponDispersion.Run_StateDispersionReduction;
		break;
	case EMovementState::Walk_State:
		CurrentDispersionMax = WeaponSettings.WeaponDispersion.Walk_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSettings.WeaponDispersion.Walk_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSettings.WeaponDispersion.DispersionRecoil;
		CurrentDispersionReduction = WeaponSettings.WeaponDispersion.Walk_StateDispersionReduction;
		break;
	case EMovementState::AimRun_State:
		CurrentDispersionMax = WeaponSettings.WeaponDispersion.AimRun_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSettings.WeaponDispersion.AimRun_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSettings.WeaponDispersion.DispersionRecoil;
		CurrentDispersionReduction = WeaponSettings.WeaponDispersion.AimRun_StateDispersionReduction;
		break;
	case EMovementState::AimWalk_State:
		CurrentDispersionMax = WeaponSettings.WeaponDispersion.AimWalk_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSettings.WeaponDispersion.AimWalk_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSettings.WeaponDispersion.DispersionRecoil;
		CurrentDispersionReduction = WeaponSettings.WeaponDispersion.AimWalk_StateDispersionReduction;
		break;
	case EMovementState::Sprint_State:
		BlockFire = true;
		SetWeaponStateFire(false);
		break;

	default:
		break;
	}
}




/*-- ��������� ��������� �� ���� --*/
void AWeaponDefault::DispersionTick(float DeltaSeconds)
{
	if (!WeaponReloading)
	{
		if (!WeaponFiring)
		{
			if (ShouldReduceDispersion)
			{
				CurrentDispersion = CurrentDispersion - CurrentDispersionReduction;
			}
			else
			{
				CurrentDispersion = CurrentDispersion + CurrentDispersionReduction;
			}
		}
		if (CurrentDispersion < CurrentDispersionMin)
		{
			CurrentDispersion = CurrentDispersionMin;
		}
		else
		{
			if (CurrentDispersion > CurrentDispersionMax)
			{
				CurrentDispersion = CurrentDispersionMax;
			}
		}
	}
	if (ShowDebug)
	{
		UE_LOG(LogTemp,Warning,TEXT("Dispersion: MAX = %f. MIN = %f. Current = %f"), CurrentDispersionMax, CurrentDispersionMin, CurrentDispersion);
	}
}




/*--------------------------- ������� --------------------------------*/

int32 AWeaponDefault::GetWeaponRound()
{
	return AdditionalWeaponInfo.Round;
}




void AWeaponDefault::InitReload()
{
	WeaponReloading = true;

	ReloadTimer = WeaponSettings.ReloadTime;	// ����������� ������� �������� �� ��������� � Types.h

	// ToDo Anim reload

	if (WeaponSettings.AnimCharacterReload)
	{
		OnWeaponReloadStart.Broadcast(WeaponSettings.AnimCharacterReload);
	}
}




void AWeaponDefault::ReloadTick(float DeltaSeconds)
{
	if (WeaponReloading)
	{
		if (ReloadTimer < 0)
		{
			FinishReload();
		}
		else
		{
			ReloadTimer -= DeltaSeconds;
		}
	}
}




void AWeaponDefault::FinishReload()
{
	WeaponReloading = false;
	AdditionalWeaponInfo.Round = WeaponSettings.MaxRound;

	OnWeaponReloadEnd.Broadcast();
}




int8 AWeaponDefault::GetNumberProjectileByShot() const
{
	return WeaponSettings.NumberProjectileByShot;
}









































